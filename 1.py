#coding: utf-8
import sys

delims = "$#@%^&?*+-/"

def delim(str):
    r = {}
    for c in delims:
        r[str.find(c)] = c
    del r[-1]
    return r[min(r.keys())]

#узел дерева
class Node(object):
    def __init__(self):
        self.suffix_node = -1
        self.common = ""

#ребро дерева
class Edge(object):
    def __init__(self, first_char_index, last_char_index, source_node_index, dest_node_index):
        self.first_char_index = first_char_index
        self.last_char_index = last_char_index
        self.source_node_index = source_node_index
        self.dest_node_index = dest_node_index

    @property
    def length(self):
        return self.last_char_index - self.first_char_index

#объекс суффикса
class Suffix(object):
    def __init__(self, source_node_index, first_char_index, last_char_index):
        self.source_node_index = source_node_index
        self.first_char_index = first_char_index
        self.last_char_index = last_char_index

    @property
    def length(self):
        return self.last_char_index - self.first_char_index

    # заканчивается ли этот суффикс на реальном узле
    def explicit(self):
        return self.first_char_index > self.last_char_index
    # заканчивается ли этот суффикс где-то на ребре
    def implicit(self):
        return self.last_char_index >= self.first_char_index

#суффиксное дерево
class SuffixTree(object):
    def __init__(self, string, count):
        self.string = string
        self.count = count
        self.N = len(string) - 1
        self.nodes = [Node()]
        self.edges = {}
        self.active = Suffix(0, 0, -1)
        self.lcss = False
        self.maxl = -1
        for i in range(len(string)):
            self.add(i)

    #все рёбра, выходящие из определённого узла
    def fedg(self, idx):
        res = []
        for i,c in self.edges.keys():
            if i == idx:
                res.append(self.edges[i,c])
        return res

    def str(self, e):
        return self.string[e.first_char_index:e.last_char_index+1]

    #печать дерева
    def to_s(self, idx = 0, depth = 0):
        ed = self.fedg(idx)
        for e in ed:
            sys.stdout.write("  " * depth)
            sys.stdout.write(self.str(e) + " ")
            print self.nodes[e.dest_node_index].common
            self.to_s(e.dest_node_index, depth + 1)

    #добавление ещё одного символа
    def add(self, last_char_index):
        last_parent_node = -1
        while True:
            parent_node = self.active.source_node_index
            # если этот суффикс на реальном узле
            if self.active.explicit():
                # если уже есть в дереве, то выходим
                if (self.active.source_node_index, self.string[last_char_index]) in self.edges:
                    break
            else:
                e = self.edges[self.active.source_node_index, self.string[self.active.first_char_index]]
                # если уже есть в дереве, то выходим
                if self.string[e.first_char_index + self.active.length + 1] == self.string[last_char_index]:
                    break
                # разобьём ребро на два
                parent_node = self.break_edge(e, self.active)

            # добавим новый узел
            self.nodes.append(Node())
            # создадим ребро до этого узла
            e = Edge(last_char_index, self.N, parent_node, len(self.nodes) - 1)
            self.add_edge(e)

            if last_parent_node > 0:
                self.nodes[last_parent_node].suffix_node = parent_node
            last_parent_node = parent_node

            # переназначим индексы
            if self.active.source_node_index == 0:
                self.active.first_char_index += 1
            else:
                self.active.source_node_index = self.nodes[self.active.source_node_index].suffix_node

            self.fix_suffix(self.active)
        if last_parent_node > 0:
            self.nodes[last_parent_node].suffix_node = parent_node
        self.active.last_char_index += 1
        # исправляем рёбра
        self.fix_suffix(self.active)

    # добавление ребра
    def add_edge(self, edge):
        self.edges[(edge.source_node_index, self.string[edge.first_char_index])] = edge

    # удаление ребра
    def remove_edge(self, edge):
        self.edges.pop((edge.source_node_index, self.string[edge.first_char_index]))

    # разбиение ребра, чтобы указанный суффикс указывал на реальный узел
    def break_edge(self, edge, suffix):
        self.nodes.append(Node())
        e = Edge(edge.first_char_index, edge.first_char_index + suffix.length, suffix.source_node_index, len(self.nodes) - 1)
        self.remove_edge(edge)
        self.add_edge(e)
        self.nodes[e.dest_node_index].suffix_node = suffix.source_node_index
        edge.first_char_index += suffix.length + 1
        edge.source_node_index = e.dest_node_index
        self.add_edge(edge)
        return e.dest_node_index

    # нормализация дерева: узлы только там, где заканчивается суффикс или если есть несколько потомков (неявное)
    def fix_suffix(self, suffix):
        if not suffix.explicit():
            e = self.edges[suffix.source_node_index, self.string[suffix.first_char_index]]
            if e.length <= suffix.length:
                suffix.first_char_index += e.length + 1
                suffix.source_node_index = e.dest_node_index
                self.fix_suffix(suffix)

    def mark_common(self, idx):
        ed = self.fedg(idx)
        strs = []
        for e in ed:
            childs = self.fedg(e.dest_node_index)
            # если e указывает на лист
            if len(childs) == 0:
                strs.append(delim(self.str(e)))
            else:
                strs = strs + self.mark_common(e.dest_node_index)
        self.nodes[idx].common = len(set(strs)) == self.count
        return strs

    def find_lcs(self, idx, depth = 0, cur_str = ""):
        ed = self.fedg(idx)
        if len(ed) == 0:
            return
        if not self.nodes[idx].common:
            return
        if depth > self.maxl:
            self.maxl = depth
            self.lcss = cur_str
        for e in ed:
            s = self.str(e)
            self.find_lcs(e.dest_node_index, depth + len(s), cur_str + s)


    def lcs(self, depth = 0):
        self.mark_common(0)
        self.find_lcs(0)
        if self.lcss:
            print "Наибольшая общая подстрока: " + self.lcss
        else:
            print "Наибольшая общая подстрока не найдена"




n = input()

res = ""
for i in range(n):
    s = raw_input()
    res += s + delims[i]

print res
t = SuffixTree(res, n)
t.to_s()
t.lcs()
