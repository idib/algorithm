package lab5.BTree;

/**
 * Created by idib on 16.11.16.
 */
public abstract class AbstractChildren {
    static double lengPointLint = 10, padding = 5;
    protected double x, y, width,height;

    public static double getLengPointLint() {
        return lengPointLint;
    }

    public static void setLengPointLint(double lengPointLint) {
        AbstractChildren.lengPointLint = lengPointLint;
    }

    public static double getPadding() {
        return padding;
    }

    public static void setPadding(double padding) {
        AbstractChildren.padding = padding;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
