package lab5.BTree;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Created by idib on 16.11.16.
 */

public class Entry<K extends Comparable<K>, V> extends AbstractChildren {
    private K key;
    private V value;
    private Text text;
    private Rectangle bounds;

    public Entry(K key, V value) {
        this.key = key;
        text = new Text();
        bounds = new Rectangle();
        setValue(value);
        bounds.setFill(Color.TRANSPARENT);
        bounds.setStroke(Color.BLACK);
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
        text.setText(value.toString());
        bounds.setWidth(text.getLayoutBounds().getWidth() + 2 * padding);
        bounds.setHeight(text.getLayoutBounds().getHeight() + 2 * padding);
        width = bounds.getWidth();
        height = bounds.getHeight();
    }

    @Override
    public void setX(double x) {
        super.setX(x);
        text.setX(x + padding);
        bounds.setX(x);
    }

    @Override
    public void setY(double y) {
        super.setY(y);
        text.setY(y + padding + text.getLayoutBounds().getHeight());
        bounds.setY(y);
    }

    public boolean less(K k1) {
        return k1.compareTo(key) > 0;
    }

    public boolean eq(K k1) {
        return k1.compareTo(key) == 0;
    }

    public boolean larger(K k1) {
        return k1.compareTo(key) < 0;
    }

    public ArrayList<Shape> getShape() {
        ArrayList<Shape> res = new ArrayList<>();
        res.add(text);
        res.add(bounds);
        return res;
    }
}
