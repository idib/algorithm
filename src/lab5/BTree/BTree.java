package lab5.BTree;

import javafx.scene.shape.Shape;

import java.util.ArrayList;

/**
 * Created by idib on 16.11.16.
 */


public class BTree<K extends Comparable<K>, V> {
    private int T = 4;

    public Node<K, V> Root;
    private double sX;
    private double sY;
    private double distX = 45;
    private double distY = 45;

    public BTree(int t) {
        T = t;
        Root = new Node(t);
        Root.leaf = true;
    }

    public double getSX() {
        return sX;
    }

    public void setSX(double sx) {
        this.sX = sx;
    }

    public double getSY() {
        return sY;
    }

    public void setSY(double sy) {
        this.sY = sy;
    }

    public void setDistX(double distX) {
        this.distX = distX;
    }

    public void setDistY(double distY) {
        this.distY = distY;
    }

    public V get(K k) {
        if (k == null) throw new IllegalArgumentException("argument to get() is null");
        return search(Root, k).getValue();
    }

    public void put(K k, V val) {
        Entry<K, V> cur = search(Root, k);
        if (cur != null) {
            cur.setValue(val);
        } else {
            Node<K, V> r = Root;
            if (r.Count == 2 * T - 1) {
                Node<K, V> s = new Node<K, V>(T);
                Root = s;
                s.Count = 0;
                s.children[0] = r;
                split(s, 0);
                insert(s, k, val);
            } else
                insert(r, k, val);
        }
    }

    public boolean containsKey(K key) {
        if (search(Root, key) == null)
            return false;
        else
            return true;
    }

    public Entry<K, V> search(Node<K, V> x, K k) {
        int i = 0;
        while (i < x.Count && x.entry[i].less(k)) i++;
        if (i < x.Count && x.entry[i].eq(k))
            return x.entry[i];
        if (x.children[i] == null)
            return null;
        return search(x.children[i], k);
    }

    private void split(Node<K, V> x, int ind) {
        Node<K, V> z = new Node<K, V>(T);
        Node<K, V> y = x.children[ind];
        z.leaf = y.leaf;
        z.Count = T - 1;
        for (int i = 0; i < z.Count; i++)
            z.entry[i] = y.entry[i + T];
        if (!y.leaf)
            for (int i = 0; i < T; i++) {
                z.children[i] = y.children[i + T];
            }
        y.Count = T - 1;
        for (int i = x.Count; i > ind; i--)
            x.children[i + 1] = x.children[i];
        x.children[ind + 1] = z;
        for (int i = x.Count; i > ind; i--)
            x.entry[i] = x.entry[i - 1];
        x.entry[ind] = y.entry[T - 1];
        x.Count++;
    }

    private void insert(Node x, K k, V val) {
        int i = x.Count;
        if (x.leaf) {
            for (int j = 0; j < i; j++)
                if (x.entry[j].eq(k))
                    return;
            for (; i > 0 && x.entry[i - 1].larger(k); i--)
                x.entry[i] = x.entry[i - 1];
            x.entry[i] = new Entry(k, val);
            x.Count++;
        } else {
            while (i > 0 && x.entry[i - 1].larger(k)) i--;
            if (x.children[i].Count == 2 * T - 1) {
                split(x, i);
                if (x.entry[i].less(k))
                    i++;
            }
            insert(x.children[i], k, val);
        }
    }

    public void del(K key) {
        if (search(Root, key) != null)
            delete(Root, key);
    }

    public K maxOnSelect(K min, K max) {
        int i = 0;
        Entry<K, V> cur = null;
        Node<K, V> x = Root;
        while (x != null) {
            i = 0;
            while (i < x.Count && (x.entry[i].less(max) || x.entry[i].eq(max))) {
                if ((x.entry[i].larger(min) && x.entry[i].less(max)) || x.entry[i].eq(max) || x.entry[i].eq(min)) {
                    if (cur == null || cur.less(x.entry[i].getKey())) {
                        cur = x.entry[i];
                    }
                }
                i++;
            }
            x = x.children[i];
        }
        if (cur == null)
            return null;
        return cur.getKey();
    }

    private void delete(Node<K, V> x, K key) {
        int Count = x.Count;
        int ind = -1;
        for (int i = 0; i < Count; i++)
            if (x.entry[i].eq(key))
                ind = i;
        if (x.leaf) {
            for (int i = ind + 1; i < Count; i++)
                x.entry[i - 1] = x.entry[i];
            x.Count--;
        } else {
            if (ind != -1) {
                if (x.children[ind].Count >= T)
                    x.entry[ind] = x.children[ind].entry[x.children[ind].Count-- - 1];
                else if (ind <= Count && x.children[ind + 1].Count >= T) {
                    x.entry[ind] = x.children[ind + 1].entry[0];
                    for (int i = 1; i < x.children[ind + 1].Count; i++)
                        x.children[ind + 1].entry[i - 1] = x.children[ind + 1].entry[i];
                    x.children[ind + 1].Count--;
                } else if (x.children[ind].Count == T - 1 && ind <= Count && x.children[ind + 1].Count == T - 1) {
                    Merge(x, ind);
                    for (int i = 0; i < T - 1; i++)
                        x.children[ind].entry[i + T - 1] = x.children[ind].entry[i + T];
                    for (int i = 0; i < T; i++)
                        x.children[ind].children[i + T - 1] = x.children[ind].children[i + T];
                    x.children[ind].Count--;
                }
            } else {
                ind = 0;
                while (ind < Count && x.entry[ind].less(key)) ind++;
                if (x.children[ind].Count == T - 1)
                    if (ind > 0 && x.children[ind - 1].Count == T)
                        Transplant(x, ind - 1);
                    else if (ind < Count && x.children[ind + 1].Count == T)
                        Transplant(x, ind);
                    else if (ind > 0 && x.children[ind - 1].Count == T - 1)
                        Merge(x, ind-- - 1);
                    else if (ind < Count && x.children[ind + 1].Count == T - 1)
                        Merge(x, ind--);
                if (ind == -1)
                    ind++;
                delete(x.children[ind], key);
            }
        }
    }

    private void Transplant(Node x, int i) {
        x.children[i].entry[T - 1] = x.entry[i];
        x.children[i].children[T] = x.children[i + 1].children[0];
        x.entry[i] = x.children[i + 1].entry[0];
        for (int j = 0; j < T; j++)
            x.children[i + 1].entry[j] = x.children[i + 1].entry[j + 1];
        for (int j = 0; j <= T; j++)
            x.children[i + 1].children[j] = x.children[i + 1].children[j + 1];
        x.children[i].Count++;
        x.children[i + 1].Count--;
    }

    private void Merge(Node x, int i) {
        x.children[i].entry[T - 1] = x.entry[i];
        for (int j = 0; j < T - 1; j++)
            x.children[i].entry[j + T] = x.children[i + 1].entry[j];
        for (int j = 0; j < T; j++)
            x.children[i].children[j + T] = x.children[i + 1].children[j];

        for (int j = i + 1; j < x.Count; j++)
            x.entry[j - 1] = x.entry[j];
        for (int j = i + 2; j <= x.Count; j++)
            x.children[j - 1] = x.children[j];

        x.Count--;
        x.children[i].Count = 2 * T - 1;
        if (Root == x && x.Count == 0) {
            Root = x.children[0];
        }
    }

    public void refreshXY() {
        if (Root != null) {
            refreshX(Root, sX);
            refreshY(Root, sY);
        }
    }

    private double refreshX(Node n, double curX) {
        double s = curX;
        double min = s;

        for (int i = 0; i <= n.Count; i++) {
            if (n.children[i] != null)
                s = refreshX(n.children[i], s + distX);
        }
        //n.setX((min + s) / 2 - n.getWidth() / 2);
        n.setX((min + s) / 2);
        return s + n.getWidth();
    }

    private void refreshY(Node n, double curY) {
        n.setY(curY);
        for (Node child : n.children)
            if (child != null)
                refreshY(child, curY + distY);
    }

    public ArrayList<Shape> getShapeTree() {
        if (Root != null)
            return getShapeNode(Root);
        return new ArrayList<>();
    }

    private ArrayList<Shape> getShapeNode(Node t) {
        ArrayList<Shape> res = new ArrayList<>();
        res.addAll(t.getShape());
        if (t.leaf)
            return res;
        for (int i = 0; i <= t.Count; i++) {
            if (t.children[i] != null)
                res.addAll(getShapeNode(t.children[i]));
        }
        return res;
    }
}