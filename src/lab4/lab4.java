package lab4;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by idib on 22.10.16.
 */
public class lab4 {


    private static JTextArea TXT;
    private static JTextField Pat1;


    private static void init() throws BadLocationException {
        JFrame frame = new JFrame();

        frame.setSize(600, 400);
        frame.setVisible(true);


        TXT = new JTextArea();

        TXT.setSize(300, 150);
        TXT.setText("arraraaa asasf aaara ararr ararrre erer");

        Pat1 = new JTextField(15);
        Pat1.setSize(150, 30);
        Pat1.setText("arra");


        JButton FindBtn = new JButton();
        FindBtn.setSize(150, 30);
        FindBtn.setText("Найти");
        FindBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FindBM();
            }
        });


        // Настраиваем вторую горизонтальную панель (для ввода пароля)
        Box box2 = Box.createHorizontalBox();
        box2.add(Pat1);
        box2.add(Box.createHorizontalStrut(6));
        box2.add(FindBtn);
        // Уточняем размеры компонентов
        // Размещаем три горизонтальные панели на одной вертикальной
        Box mainBox = Box.createVerticalBox();
        //mainBox.setBorder(new EmptyBorder(12,12,12,12));
        mainBox.add(TXT);
        mainBox.add(Box.createVerticalStrut(12));
        mainBox.add(box2);
        frame.setContentPane(mainBox);
        frame.setTitle("SufAuto");
    }


    private static void FindBM() {
        String pat = Pat1.getText();
        String txt = TXT.getText();
        String[] as = pat.split(" ");
        TXT.getHighlighter().removeAllHighlights();

        SufTree ttts = new SufTree(txt);
        ttts.print();

        for (int i = 0; i < as.length; i++) {
            int res = ttts.find(as[i]);
            try {
                TXT.getHighlighter().addHighlight(res, res + as[i].length(), new DefaultHighlighter.DefaultHighlightPainter(Color.red));
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

//        int s = 0;
//        while (s < res.size()) {
//            try {
//                TXT.getHighlighter().addHighlight(res.get(s) + 1, res.get(s) + 1 + pat.length(), new DefaultHighlighter.DefaultHighlightPainter(Color.red));
//            } catch (BadLocationException e) {
//                e.printStackTrace();
//            }
//            s++;
//        }

        TXT.setCaretPosition(0);
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    init();
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
