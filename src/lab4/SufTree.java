package lab4;

import java.util.HashMap;
import java.util.Stack;

// BBBBBABABBBAABBBB PABCQRABCSABTU ABCDABC AAAAAA ABCCBA

// Суффиксное дерево, построение - упрощённый вариант алгоритма Вейнера
// Для заданной строки найти самую длинную подстроку, встречающуюся в y более одного раза
public class SufTree {

    // Строка, по которой построено дерево
    private String string;
    // Фейковая (используется для исключения крайних случаев), корневая и последняя добавленная вершины
    private Node fake = new Node(), root = new Node(-1, 1, fake), last = root;

    // Конструктор, принимающий исходную строку
    public SufTree(String s) {
        // Добавляем специальный символ в конец строки и запоминаем её
        string = s + "$";
        // Добавляем суффиксные ссылки из fake по всем буквам в корневую вершину
        for (char i = 0; i < 256; i++) fake.link.put(i, root);

        // Пошаговое построение дерева, начиная с конца строки
        for (int i = string.length() - 1; i >= 0; i--) extend(i);
    }

    // Один шаг алгоритма
    public void extend(int i) {
        Node v = last;
        int vlen = string.length() - i;

        Stack<Node> path = new Stack<Node>();            // Поднимаемся от последней вершины
        while (!v.link.containsKey(string.charAt(i))) {    // Пока не нашли префиксную ссылку
            vlen -= v.len;
            path.push(v);

            v = v.parent;
        } // В конце vlen = |str(v)| + 1 / str(v) - длина строки на пути от корня до v

        Node w = v.link.get(string.charAt(i));            // Переходим по префиксной ссылке
        if (w.to.containsKey(string.charAt(i + vlen))) {    // Если w != w'
            Node u = w.to.get(string.charAt(i + vlen)), nw = new Node();

            for (nw.pos = u.pos - u.len; string.charAt(nw.pos) == string.charAt(i + vlen); nw.pos += v.len) {
                v = path.pop();                            // Определяем место разбиения ребра
                vlen += v.len;                            // ^ очередной кандидат на v'
            }

            attach(nw, w, string.charAt(u.pos - u.len), u.len - (u.pos - nw.pos));    // Присоединить w' к v
            attach(u, nw, string.charAt(nw.pos), u.pos - nw.pos);                    // Присоединить u к w'

            v.link.put(string.charAt(i), nw);    // Провести префиксную ссылку из v' в w'
            w = nw;                                // Установить w = w' для вставки нового листа
        }

        Node leaf = new Node(string.length());    // Новый лист, pos для листьев всегда длина строки

        last.link.put(string.charAt(i), leaf);    // Префиксная ссылка из старого листа к новому
        attach(leaf, w, string.charAt(i + vlen), string.length() - (i + vlen));    // Присоединить новый лиит к w'

        last = leaf;
    }

    // Вспомогательный метод связи двух вершин дерева
    public void attach(Node child, Node parent, char c, int cl) {
        parent.to.put(c, child);

        child.len = cl;
        child.parent = parent;
    }

    // Вывод текущего суффиксного дерева
    public void print() {
        System.out.println("\nSuffix tree:");
        print(root, 0);
    }

    // Вывод узла дерева и его потомков с заданным смещением
    public void print(Node node, int offset) {
        for (int i = 0; i < offset; i++) System.out.print("--");

        String s = (node.pos == -1) ? "" : string.substring(node.pos - node.len, node.pos);
        System.out.println("> " + node.index + " " + s);

        for (Node next : node.to.values()) print(next, offset + 1);
    }

    public int find(String str) {
        Node cur = root;
        int i;
        boolean fl = true;
        for (i = 0; fl && i < str.length(); i++) {
            if (cur.to.containsKey(str.charAt(i)))
                cur = cur.to.get(str.charAt(i));
            else
                fl = false;
        }
        if (!fl) {
            for (int j = i; i != -1 && j < str.length(); j++) {
                if (str.charAt(j) != string.charAt(cur.pos - cur.len + j))
                    i = - 1;
            }
            i -= 1;
        }
        if (i != -1)
            return cur.pos - cur.len - i + 1;
        return -1;

    }

    // Вспомогательный класс описания вершины дерева
    private static class Node {
        // Количество вершин в дереве
        public static int count = 0;
        // Позиция и длина подстроки ребра перехода
        public int index = count++, pos = 0, len = 0;
        // Родительская вершина
        public Node parent = null;
        // Ссылки на детей вершины
        public HashMap<Character, Node> to = new HashMap<Character, Node>();
        // Префиксные ссылки
        public HashMap<Character, Node> link = new HashMap<Character, Node>();

        public Node() {
        }

        public Node(int p) {
            pos = p;
        }

        public Node(int p, int l, Node pr) {
            pos = p;
            len = l;
            parent = pr;
        }
    }
}