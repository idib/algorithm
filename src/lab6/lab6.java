package lab6;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by idib on 11.11.16.
 */
public class lab6 {
    private static JTextArea TXT;
    private static JTextArea Pat1;


    private static void init() throws BadLocationException {
        JFrame frame = new JFrame();

        frame.setSize(600, 400);
        frame.setVisible(true);


        TXT = new JTextArea();

        TXT.setSize(300, 150);
        TXT.setText("1 2 3 4 5 6 7 8 9 1 2 36 7 8 9 15 313 13 15 16 19 20 55 6 64 67");

        Pat1 = new JTextArea();
        Pat1.setSize(150, 30);


        JButton FindBtn = new JButton();
        FindBtn.setSize(150, 30);
        FindBtn.setText("Найти");
        FindBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FindBM();
            }
        });


        // Настраиваем вторую горизонтальную панель (для ввода пароля)
        Box box2 = Box.createHorizontalBox();
        box2.add(Pat1);
        box2.add(Box.createHorizontalStrut(6));
        box2.add(FindBtn);
        // Уточняем размеры компонентов
        // Размещаем три горизонтальные панели на одной вертикальной
        Box mainBox = Box.createVerticalBox();
        //mainBox.setBorder(new EmptyBorder(12,12,12,12));
        mainBox.add(TXT);
        mainBox.add(Box.createVerticalStrut(12));
        mainBox.add(box2);
        frame.setContentPane(mainBox);
        frame.setTitle("SufAuto");
    }


    private static void FindBM() {
        String txt = TXT.getText();

        ArrayList<Integer> nums = Arrays.stream(txt.split(" ")).map(T -> Integer.parseInt(T)).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<Integer> numC  = nums.stream().map(T-> 0).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<Integer> link  = nums.stream().map(T-> -1).collect(Collectors.toCollection(ArrayList::new));
        for(int i = 1 ; i < nums.size(); i++)
        {
            for(int j = i; j >=0; j--)
            {
                if (nums.get(i) > nums.get(j) && numC.get(i) < numC.get(j) + 1)
                {
                    numC.set(i,numC.get(j) + 1);
                    link.set(i,j);
                }
            }
        }


        ArrayList<Integer> ri = new ArrayList<>();
        int max = 0;
        for(int i = 0 ; i < nums.size();i++)
        {
            if (numC.get(i) > numC.get(max))
                max = i;
        }

        while (max >= 0) {
            ri.add(nums.get(max));
            max = link.get(max);
        }

        String res = "Max len: " + ri.size() + '\n';
        for (int i = ri.size() - 1; i >= 0; i--) {
            res += ri.get(i).toString() + ' ';
        }

        Pat1.setText(res);
        System.out.println(ri.size());
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    init();
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }

            }
        });
//
//        AutoStr t = new AutoStr();
//        String inputs = in.next();
//        //String inputs = "kwgkjasjabcdbfkjbabcabc";
//        t.str(inputs);
//        String S = in.next();
//        ArrayList<Integer> res = t.find(S);
//        System.out.println(inputs);
//        int sz = 0;
//        for (int i = 0; i < inputs.length() && sz < res.size(); i++) {
//            if (res.get(sz) + 1 == i) {
//                System.out.print("^");
//                sz++;
//            } else
//                System.out.print(" ");
//        }
//        System.out.println();
//        sz = 0;
//        for (int i = 0; i < inputs.length() && sz < res.size(); i++) {
//            if (res.get(sz) + 1 == i) {
//                System.out.print("|");
//                sz++;
//            } else
//                System.out.print(" ");
//        }
//        return;
    }
}
