package lab9;

import com.sun.org.apache.xpath.internal.operations.Equals;

import java.util.ArrayList;

public class BigNum {

    public static final int base = 1000 * 1000 * 1000;
    public static final int precision = 2;

    private ArrayList<Integer> data = new ArrayList<Integer>();


    private BigNum() {
    }


    public BigNum(int a) {

        for (int i = 0; i < precision; i++) data.add(0);

        data.add(a);
    }


    public BigNum(double a) {

        this((int) a);

        String s = Double.toString(a - (int) a + 1).substring(2);


        int at = precision - 1;
        while (!s.equals("") && at >= 0) {
            String add;
            if (s.length() <= 9) {

                add = s;
                for (int i = 9 - s.length(); i > 0; i--)
                    add += "0";
                s = "";
            } else {

                add = s.substring(0, 9);
                s = s.substring(9);
            }


            data.set(at--, Integer.parseInt(add));
        }
    }


    public BigNum add(BigNum b) {
        BigNum result = new BigNum();

        boolean carry = false;

        for (int i = 0; i < Math.max(data.size(), b.data.size()) || carry; i++) {

            if (i == result.data.size()) result.data.add(0);

            result.data.set(i, (i < data.size() ? data.get(i) : 0) + (carry ? 1 : 0) + (i < b.data.size() ? b.data.get(i) : 0));

            carry = result.data.get(i) >= base;
            if (carry)
                result.data.set(i, result.data.get(i) - base);
        }
        return result;
    }

    public BigNum multiply(BigNum b) {
        BigNum result = new BigNum();
        for (int i = 0; i < data.size(); i++) {
            long carry = 0;
            for (int j = 0; j < b.data.size() || carry != 0; j++) {
                int digit = i + j - precision;
                if (digit < 0)
                    continue;

                if (digit == result.data.size())
                    result.data.add(0);
                long val = result.data.get(digit) + data.get(i) * 1L * (j < b.data.size() ? b.data.get(j) : 0) + carry;
                result.data.set(digit, (int) (val % base));
                carry = val / base;
            }
        }
        return result;
    }

    public BigNum power(int n) {
        if (n == 0)
            return new BigNum(1);
        if (n % 2 == 1)
            return power(n - 1).multiply(this);
        else {
            BigNum b = power(n / 2);
            return b.multiply(b);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(data.get(data.size() - 1));
        for (int i = data.size() - 2; i >= precision; i--)
            builder.append(String.format("%09d", data.get(i)));

        builder.append(".");
        for (int i = precision - 1; i >= 0; i--)
            builder.append(String.format("%09d", data.get(i)));

        String s = builder.toString();
        int i = 0;
        for (i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == '.') break;
            if (s.charAt(i) != '0') {
                i++;
                break;
            }
        }

        return s.substring(0, i);
    }

    public boolean equals(BigNum b) {
        return data.equals(b.data);
    }
}