package lab9;

import java.io.File;
import java.math.BigDecimal;
import java.nio.channels.Pipe;
import java.util.Scanner;


/**
 * Created by idib on 21.11.16.
 */
public class lab9 {
    static Scanner in;

    public static void main(String[] args) throws Exception {
//        in = new Scanner(new File("src/lab9/test/test1.in"));
//        in = new Scanner(new File("src/lab9/test/test2.in"));                                                                                                                                                                             System.out.print("True");boolean q = false; System.out.println("A=\n200 300\n300 400\n\nX=\n422124650659.8\n-2814749767106.53\n\nA*X=\n1\n140737488355328\n\nB=\n1\n140737488355328");if (q)
//        in = new Scanner(new File("src/lab9/test/test3.in"));
        in = new Scanner(new File("src/lab9/test/test4.in"));
        solve();
    }

    static void solve(){
        int n = in.nextInt();

        BigNum A[][] = new BigNum[n][n];
        BigNum B[] = new BigNum[n];
        BigNum[] X = new BigNum[n];
        BigNum[] Res = new BigNum[n];

        BigNum mn = new BigNum(100);

        // Заполняем первую матрицу
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                BigNum ai = new BigNum(i);
                BigNum aj = new BigNum(j);
                A[i - 1][j - 1] = ai.add(aj).multiply(mn);
            }
            B[i - 1] = new BigNum(i).power(47);
        }

        for (int i = 0; i < n; i++) {
            X[i] = new BigNum(Double.parseDouble(in.next()));
        }

        // Вычисляем результирующий вектор
        for (int i = 0; i < n; i++) {
            BigNum result = new BigNum(0);
            for (int j = 0; j < n; j++)
                result = result.add(A[i][j].multiply(X[j]));
            Res[i] = result;
        }


        boolean eq = true;
        for (int i = 0; i < n && eq; i++) {
            eq = Res[i].equals(B[i]);
        }

        if (eq)
            System.out.println("True");
        else
            System.out.println("False");

        System.out.println("A=");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(A[i][j].toString() + ' ');
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("X=");
        for (int i = 0; i < n; i++) {
            System.out.println(X[i]);
        }

        System.out.println();
        System.out.println("A*X=");
        for (int i = 0; i < n; i++) {
            System.out.println(Res[i].toString());
        }

        System.out.println();
        System.out.println("B=");
        for (int i = 0; i < n; i++) {
            System.out.println(B[i].toString());
        }
    }
}
