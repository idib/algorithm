package lab3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by idib on 04.11.16.
 */

public class AutoStr {
    class state {
        public int len, link;
        public HashMap<Character, Integer> next;


        public boolean is_clon;
        public int first_pos;
        public ArrayList<Integer> inv_link;

        public state() {
            next = new HashMap<>();
            inv_link = new ArrayList<>();
        }

        public String print(){
            return "->" + next.toString();
        }

    }

    private static final int MAXLEN = 100000;

    private state[] st;
    private ArrayList<Integer> firstIn;
    private int sz, last;

    public AutoStr() {
        init();
    }

    public AutoStr(String s) {
        str(s);
    }

    private void init() {
        st = new state[MAXLEN * 2];
        firstIn = new ArrayList<>();
        for (int i = 0; i < MAXLEN; i++) {
            st[i] = new state();
        }
        sz = last = 0;
        st[0].len = 0;
        st[0].link = -1;
        ++sz;
    }

    public void str(String s) {
        init();
        for (int i = 0; i < s.length(); i++) {
            addChar(s.charAt(i));
        }

        for (int v = 1; v < sz; ++v)
            st[st[v].link].inv_link.add(v);

        for (int i = 0 ; i < sz; i++) {
            System.out.println(i +  st[i].print());
        }
    }

    public void addChar(char c) {
        int cur = sz++;
        st[cur].len = st[last].len + 1;
        st[cur].first_pos = st[cur].len - 1;
        int p;
        for (p = last; p != -1 && !st[p].next.containsKey(c); p = st[p].link)
            st[p].next.put(c, cur);
        if (p == -1)
            st[cur].link = 0;
        else {
            int q = st[p].next.get(c);
            if (st[p].len + 1 == st[q].len)
                st[cur].link = q;
            else {
                int clone = sz++;
                st[clone].first_pos = st[p].first_pos;
                st[clone].is_clon = true;
                st[clone].len = st[p].len + 1;
                st[clone].next = (HashMap<Character, Integer>) st[q].next.clone();
                st[clone].link = st[q].link;
                for (; p != -1; p = st[p].link)
                {
                    int t = 0;
                    if (st[p].next.containsKey(c))
                        t = st[p].next.get(c);

                    if (t == q)
                        st[p].next.put(c, clone);
                    else
                        break;
                }
                st[q].link = st[cur].link = clone;
            }
        }
        last = cur;
    }

    public ArrayList<Integer> find(String s) {

        int FP = 0;
        for (int i = 0; i < s.length(); i++) {
            FP = st[FP].next.get(s.charAt(i));
        }

        output_all_occurences(FP);
        ArrayList<Integer> res = (ArrayList<Integer>) firstIn.clone();
        firstIn.clear();
        return res.stream().map(T -> T - s.length()).collect(Collectors.toCollection(ArrayList::new));
    }


    void output_all_occurences(int v) {
        if (!st[v].is_clon)
            firstIn.add(st[v].first_pos);
        for (int i = 0; i < st[v].inv_link.size(); ++i)
            output_all_occurences(st[v].inv_link.get(i));
    }
}
