package lab7;

import java.util.Scanner;

/**
 * Created by idib on 15.11.16.
 */
public class lab7 {
    public static boolean mask[];
    public static int res[];
    public static int alf[];
    public static int N;
    public static int count;

    public static void print(){
        count++;
        for (int re : res) {
            System.out.print(Integer.toString(re) + ' ');
        }
        System.out.println();
    }


    public static void nextVar(int pos){
        if (pos >= N)
        {
            print();
        }
        else {
            if (mask[pos])
                nextVar(pos + 1);
            else
                for (int i = 0; i < alf.length; i++) {
                    res[pos] = alf[i];
                    nextVar(pos + 1);
                }
        }
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int t = cin.nextInt();
        alf = new int[t];
        N = cin.nextInt();
        res = new int[N];
        mask = new boolean[N];
        for (int  i = 0; i < t; i++)
        {
            alf[i] = cin.nextInt();
        }
        for(int i = 0; i < N; i++)
        {
            int id = cin .nextInt();
            if (id == -1) {
                mask[i] = false;
                res[i] = alf[0];
            }
            else
            {
                mask[i] = true;
                res[i] = id;
            }
        }
        count = 0;
        nextVar(0);
        System.out.println(count);
    }
}

/*
6
3
1 2 3 4 5 7
2 -1 -1
 */