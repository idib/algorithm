package lab2;
/**
 * Created by idib on 11.10.16.
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;


public class Lab2 {


    private static JTextArea TXT;
    private static JTextField Pat1;
    private static JTextField Pat2;


    private static void init() throws BadLocationException {
        JFrame frame = new JFrame();

        frame.setSize(600, 400);
        frame.setVisible(true);


        TXT = new JTextArea();

        TXT.setSize(300, 150);
        TXT.setText("arraraaa aarrr rrrsaaa  arrarrr");

        Pat1 = new JTextField(15);
        Pat1.setSize(150, 30);
        Pat1.setText("arra");

        Pat2 = new JTextField(15);
        Pat2.setSize(150, 30);
        Pat2.setText("rrar");

        JButton FindBtn = new JButton();
        FindBtn.setSize(150,30);
        FindBtn.setText("Найти");
        FindBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FindBM();
            }
        });




// Настраиваем вторую горизонтальную панель (для ввода пароля)
        Box box2 = Box.createHorizontalBox();
        box2.add(Pat1);
        box2.add(Box.createHorizontalStrut(6));
        box2.add(Pat2);
        box2.add(Box.createHorizontalStrut(6));
        box2.add(FindBtn);
// Уточняем размеры компонентов
// Размещаем три горизонтальные панели на одной вертикальной
        Box mainBox = Box.createVerticalBox();
        //mainBox.setBorder(new EmptyBorder(12,12,12,12));
        mainBox.add(TXT);
        mainBox.add(Box.createVerticalStrut(12));
        mainBox.add(box2);
        frame.setContentPane(mainBox);
        frame.setTitle("BoyerMoores");


    }


    private static void FindBM() {
        String pat = Pat1.getText();
        String pat2 = Pat2.getText();
        String txt = TXT.getText();


        BoyerMoore boyerMoore1 = new BoyerMoore(pat);
        BoyerMoore boyerMoore2 = new BoyerMoore(pat2);
        BoyerMoore boyerMooreAnd = new BoyerMoore(pat, pat2);

        int s = -1;

        TXT.getHighlighter().removeAllHighlights();

        String pat3 = BoyerMoore.longestCS(pat, pat2);
        while ((s = boyerMooreAnd.search(txt, s + 1)) >= 0) {
            try {
                TXT.getHighlighter().addHighlight(s, s + pat3.length(),new DefaultHighlighter.DefaultHighlightPainter(Color.red));
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        s = -1;

        while ((s = boyerMoore1.search(txt, s + 1)) >= 0) {
            try {
                TXT.getHighlighter().addHighlight(s, s + pat.length(),new DefaultHighlighter.DefaultHighlightPainter(Color.yellow));
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        s = -1;

        while ((s = boyerMoore2.search(txt, s + 1)) >= 0) {
            try {
                TXT.getHighlighter().addHighlight(s, s + pat2.length(),new DefaultHighlighter.DefaultHighlightPainter(Color.green));
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        TXT.setCaretPosition(s);
    }



    public static void main(final String[] args) throws MalformedURLException {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    init();
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}

