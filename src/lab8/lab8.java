package lab8;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by idib on 17.11.16.
 */
public class lab8 extends Application {
    ObservableList<Node> allPoints;
    double Radius = 10;
    private ArrayList<Point2D> p, c;
    private ArrayList<Ellipse> ell;

    private static double cross(Point2D a, Point2D b, Point2D c) {
        return (b.getX() - a.getX()) * (c.getY() - a.getY()) - (b.getY() - a.getY())
                * (c.getX() - a.getX());
    }

    private static double cross2(Point2D a, Point2D b) {
        return a.getX() * b.getY() - a.getY() * b.getX();
    }

    private static double dot(Point2D a, Point2D b) {
        return a.getX() * b.getX() + a.getY() * b.getY();
    }

    private static double dist(Point2D a, Point2D b) {
        return Math.sqrt((a.getX() - b.getX()) * (a.getX() - b.getX()) + (a.getY() - b.getY())
                * (a.getY() - b.getY()));
    }

    private static double dist2(Point2D a) {
        return Math.sqrt(a.getX() * a.getX() + a.getY() * a.getY());
    }

    public static void main(final String[] args) {
        launch(args);
    }

    private double angle(Point2D a, Point2D b) {
        return Math.acos(dot(a, b) / (dist2(a) * dist2(b)));
    }

    private Point2D divide(Point2D a, double x) {
        return new Point2D(a.getX() / x, a.getY() / x);
    }

    public void compute() {
        p.sort((T1, T2) -> {
                    if (T1.getX() == T2.getX())
                        return ((Double) T1.getY()).compareTo(T2.getY());
                    return ((Double) T1.getX()).compareTo(T2.getX());
                }
        );
        c = new ArrayList<Point2D>();

        // Monotone chain
        for (int i = 0; i < p.size(); i++) {
            while (c.size() >= 2
                    && cross(c.get(c.size() - 2), c.get(c.size() - 1), p.get(i)) <= 0)
                c.remove(c.size() - 1);
            c.add(p.get(i));
        }

        for (int i = p.size() - 1, t = c.size() + 1; i >= 0; i--) {
            while (c.size() >= t
                    && cross(c.get(c.size() - 2), c.get(c.size() - 1), p.get(i)) <= 0)
                c.remove(c.size() - 1);
            c.add(p.get(i));
        }

        // Rotating calipers
        int l = 1, r = 1, u = 1, n = c.size() - 1;
        long area = Long.MAX_VALUE;

        for (int i = 0; i < n; i++) {
            Point2D edge = divide(c.get((i + 1) % n).subtract(c.get(i))
                    , dist(c.get((i + 1) % n), c.get(i)));

            while (dot(edge, c.get(r % n).subtract(c.get(i))) < dot(edge, c
                    .get((r + 1) % n).subtract(c.get(i))))
                r++;
            while (u < r
                    || cross2(edge, c.get(u % n).subtract(c.get(i))) < cross2(
                    edge, c.get((u + 1) % n).subtract(c.get(i))))
                u++;
            while (l < u
                    || dot(edge, c.get(l % n).subtract(c.get(i))) > dot(edge, c
                    .get((l + 1) % n).subtract(c.get(i))))
                l++;

            double w = dot(edge, c.get(r % n).subtract(c.get(i)))
                    - dot(edge, c.get(l % n).subtract(c.get(i)));
            double h = Math.abs(cross2(c.get(i).subtract(c.get(u % n)),
                    c.get((i + 1) % n).subtract(c.get(u % n)))
                    / (dist(c.get(i), c.get((i + 1) % n))));
        }
    }

    void refresh() {
        p = allPoints.stream()
                .filter(T -> T instanceof Group)
                .map(P -> new Point2D(((Group) P).getChildren().get(0).getTranslateX(), ((Group) P).getChildren().get(0).getTranslateY()))
                .collect(Collectors.toCollection(ArrayList::new));
        if (p.size() >= 3) {
            compute();
            if (c.size() > 0) {
                for (Node aP : allPoints) {
                    if (aP instanceof Group)
                        ((Ellipse) ((Group) aP).getChildren().get(0)).setFill(Color.RED);
                }
                for (int i = 1; i < c.size(); i++) {
                    allPoints.add(getLP(c.get(i - 1).getX(), c.get(i - 1).getY(), c.get(i).getX(), c.get(i).getY()));
                    for (Node aP : allPoints) {
                        if (aP instanceof Group)
                            if (((Ellipse) ((Group) aP).getChildren().get(0)).getTranslateX() == c.get(i).getX() &&
                                    ((Ellipse) ((Group) aP).getChildren().get(0)).getTranslateY() == c.get(i).getY())
                                ((Ellipse) ((Group) aP).getChildren().get(0)).setFill(Color.GREEN);

                    }
                }
            }
        }
    }

    private Line getLP(double sX, double sY, double eX, double eY) {
        double deg = Math.atan2(eY - sY, eX - sX);
        double neX = Radius * Math.cos(deg);
        double neY = Radius * Math.sin(deg);
        if ((sX - eX) * (sX - eX) + (sY - eY) * (sY - eY) <= 4 * Radius * Radius)
            return new Line(-20,-20,-20,-20);
        return new Line(eX - neX, eY - neY, sX + neX, sY + neY);
    }

    public Node CreatePoint() {
        Ellipse res = new Ellipse();
        res.setTranslateX(15);
        res.setTranslateY(15);
        res.setRadiusX(Radius);
        res.setRadiusY(Radius);
        res.setFill(Color.TRANSPARENT);
        res.setStroke(Color.BLACK);
        return makeDraggable(res);
    }

    public void NewPoint() {
        allPoints.add(CreatePoint());
    }

    public void ClearBoard() {
        for (int i = 0; i < allPoints.size(); i++) {
            if (allPoints.get(i) instanceof Group) {
                ((Ellipse) ((Group) allPoints.get(i)).getChildren().get(0)).setFill(Color.TRANSPARENT);
            } else {
                allPoints.remove(i);
                i--;
            }
        }
    }

    @Override
    public void start(final Stage stage) {
        ell = new ArrayList<>();

        final Pane panelsPane = new Pane();
        allPoints = panelsPane.getChildren();

        final BorderPane sceneRoot = new BorderPane();


        final Button NewPnt = new Button("New Point");
        NewPnt.setOnAction(event -> NewPoint());
        BorderPane.setMargin(NewPnt, new Insets(6));
        sceneRoot.setTop(NewPnt);

        BorderPane.setAlignment(panelsPane, Pos.TOP_LEFT);
        sceneRoot.setCenter(panelsPane);


        final Scene scene = new Scene(sceneRoot, 500, 400);
        stage.setScene(scene);
        stage.setTitle("lab8");
        stage.show();
    }

    private Node makeDraggable(final Node node) {
        final DragContext dragContext = new DragContext();
        final Group wrapGroup = new Group(node);

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_CLICKED,
                mouseEvent -> {
                    if (mouseEvent.getButton() == MouseButton.SECONDARY) {
                        for (int i = 0; i < allPoints.size(); i++) {
                            if (allPoints.get(i) == wrapGroup)
                                allPoints.remove(i);
                        }
                        ClearBoard();
                        refresh();
                    }
                }
        );

        wrapGroup.addEventFilter(
                MouseEvent.ANY,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                        mouseEvent.consume();
                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                        // remember initial mouse cursor coordinates
                        // and node position
                        dragContext.mouseAnchorX = mouseEvent.getX();
                        dragContext.mouseAnchorY = mouseEvent.getY();
                        dragContext.initialTranslateX =
                                node.getTranslateX();
                        dragContext.initialTranslateY =
                                node.getTranslateY();
                        ClearBoard();
                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                mouseEvent -> {
                    // shift node from its initial position by delta
                    // calculated from mouse cursor movement
                    node.setTranslateX(
                            dragContext.initialTranslateX
                                    + mouseEvent.getX()
                                    - dragContext.mouseAnchorX);
                    node.setTranslateY(
                            dragContext.initialTranslateY
                                    + mouseEvent.getY()
                                    - dragContext.mouseAnchorY);
                    ClearBoard();
                    refresh();
                });
        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_RELEASED,
                event -> refresh()
        );
        return wrapGroup;
    }

    private static final class DragContext {
        public double mouseAnchorX;
        public double mouseAnchorY;
        public double initialTranslateX;
        public double initialTranslateY;
    }
}
